# Nautilus ROS básico

Workspace: ros_intro
Package: package

### talker.py

Implementa o nó 'signal_emitter' que envia um float escolhido aleatoriamento no intervalo [-1, 1] no tópico 'emitter'.

### listener.py

Implementa o nó 'signal_evaluator' que recebe o float enviado pelo outro nó no tópico 'emitter' e define o status True se o sinal estiver contido em [-0.75, 0.75] e False se o sinal for menor do que -0.75 ou maior do que 0.75. Adiciona o sinal e seu status no log do rospy.
