#!/usr/bin/env python3

import rospy
from std_msgs.msg import Float32


class SignalEvaluator:
    def __init__(self):
        rospy.init_node('signal_evaluator', anonymous=True)
        self.sub = rospy.Subscriber('emitter', Float32, self.callback)

        rospy.spin()
    
    def callback(self, msg):
        signal = msg.data
        
        if signal > 0.75 or signal < - 0.75:
            status = False
        else:
            status = True

        # Adiciona o sinal e seu status no log do rospy
        rospy.loginfo("Sinal: %s | Status do sinal: %s", signal, status)


if __name__ == '__main__':
    try:
        s = SignalEvaluator()
    except rospy.ROSInterruptException:
        pass
