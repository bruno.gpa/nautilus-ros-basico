#!/usr/bin/env python3

import rospy
import random
from std_msgs.msg import Float32


class SignalEmitter:
    def __init__(self):
        rospy.init_node('signal_emitter', anonymous=True)               # Inicia o nó
        self.pub = rospy.Publisher('emitter', Float32, queue_size=10)   # Cria o publisher 'emitter'

    def start(self):
        while not rospy.is_shutdown():
            rate = rospy.Rate(10)
            signal = Float32()
            signal.data = random.uniform(-1, 1)         # Float aleatório em [-1, 1] 
            self.pub.publish(signal)                    # Envia o sinal

            rate.sleep()


if __name__ == '__main__':
    try:
        s = SignalEmitter()
        s.start()
    except rospy.ROSInterruptException:
        pass
